#### Config file
# Sets application config parameters depending on `env` name
exports.setEnvironment = (env) ->
    console.log "set app environment: #{env}"
    switch(env)
        when "development"
            exports.DEBUG_LOG = true
            exports.DEBUG_WARN = true
            exports.DEBUG_ERROR = true
            exports.DEBUG_CLIENT = true
            exports.DB_HOST = 'localhost'
            exports.DB_PORT = "27017"
            exports.DB_NAME = 'musicwebsite'
            exports.DB_USER = 'angelo'
            exports.DB_PASS = ''
        when "testing"
            exports.DEBUG_LOG = true
            exports.DEBUG_WARN = true
            exports.DEBUG_ERROR = true
            exports.DEBUG_CLIENT = true
            exports.DB_HOST = 'localhost'
            exports.DB_PORT = "*****"
            exports.DB_NAME = 'music'
            exports.DB_USER = '*****'
            exports.DB_PASS = '*****'
        when "production"
            exports.DEBUG_LOG = false
            exports.DEBUG_WARN = false
            exports.DEBUG_ERROR = true
            exports.DEBUG_CLIENT = false
            exports.DB_HOST = 'localhost'
            exports.DB_PORT = "*****"
            exports.DB_NAME = 'music'
            exports.DB_USER = '*****'
            exports.DB_PASS = '*****'
        else
            console.log "environment #{env} not found"
