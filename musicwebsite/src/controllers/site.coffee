# Just renders the website :)
album = require '../models/album'
concert = require '../models/concert'
news = require '../models/news'
song = require '../models/song'

get_context = (next) ->
  context =
    'title': 'Angelo Romano'
  promise = concert.Concert.get_upcoming_concerts()
  promise.onFulfill (resp) ->
    if (!resp)
      context['upcoming_concerts'] = []
    else
      context['upcoming_concerts'] = (entry.get_properties() for entry in resp)
    next context

render_callback = (res, path) ->
  next = (context) ->
    res.render path, context
  get_context next

module.exports =
  about_get: (req, res) ->
    render_callback res, 'infos/about'

  music_get: (req, res) ->
    next = (context) ->
      promise = album.Album.get_albums()
      promise.onFulfill (resp) ->
        if (!resp)
          context['albums'] = []
        else
          context['albums'] = (entry.get_properties() for entry in resp)
        res.render 'infos/music', context
    get_context next

  live_get: (req, res) ->
    next = (context) ->
      if !req.params.id
        promise = concert.Concert.get_past_concerts()
        promise.onFulfill (resp) ->
          if (!resp)
            context['past_concerts'] = []
          else
            context['past_concerts'] = (entry.get_properties() for entry in resp)
          res.render 'infos/live', context
      else
        promise = concert.Concert.get_one_concert req.params.id
        promise.onFulfill (resp) ->
          if resp
            context['concert'] = resp.get_properties()
          res.render 'infos/live_one', context

    get_context next

  contact_get: (req, res) ->
    render_callback res, 'infos/contact'

  song_get: (req, res) ->
    next = (context) ->
      if !req.params.id
        render_callback res, 'infos/song'
      else
        promise = song.Song.get_one_song req.params.id
      promise.onFulfill (resp) ->
        if resp
          context['song'] = resp.get_properties()

        res.render 'infos/song', context
    get_context next

  news_get: (req, res) ->
    next = (context) ->
      if !req.params.id or req.params.id == 'all'
        multiple_news = true
        promise = news.News.get_news()
      else
        multiple_news = false
        promise = news.News.get_one_news req.params.id
      promise.onFulfill (resp) ->
        console.log resp
        if resp
          context['entries'] = (entry.get_properties() for entry in resp)
        else
          context['entries'] = []

        res.render 'infos/news', context
    get_context next

  equipment_get: (req, res) ->
    render_callback res, 'infos/equipment'
