moment = require 'moment'
mongoose = require 'mongoose'
utils = require '../utils'

Schema = mongoose.Schema
ObjectID = Schema.ObjectID

songSchema = new Schema
  title: String
  slug: String
  description: String
  lyrics: [String]
  urls:
    bandcamp: String
    soundcloud: String
    jamendo: String
    viinyl: String
  albums: [
    slug: String
    title: String]

# get_properties - translate to context-like object
songSchema.methods.get_properties = ->
  response =
    title: @title
    slug: @slug
    description: @description
    lyrics: utils.strarray_to_htmltext @lyrics
    urls: utils.dict_without_empty_items @urls
    albums: utils.listify @albums

  response

songSchema.statics.get_songs = ->
  promise = @
    .find({})
    .sort({title: 'asc'})
    .exec()
  promise
  
songSchema.statics.get_one_song = (slug) ->
  promise = @
    .findOne({slug: slug})
    .exec()
  promise

module.exports.Song = mongoose.model 'Song', songSchema
