moment = require 'moment'
mongoose = require 'mongoose'
utils = require '../utils'

Schema = mongoose.Schema
ObjectID = Schema.ObjectID

newsSchema = new Schema
  title: String
  content: [String]
  date: 
    type: Date
    default: Date.now
  slug: String
  categories: [String]

# get_properties - translate to context-like object
newsSchema.methods.get_properties = ->
  response =
    title: @title
    content: utils.strarray_to_htmltext @content
    slug: @slug
    categories: @categories
    date: moment(@date).format 'DD MMMM YYYY'
  response

newsSchema.statics.get_news = ->
  promise = @
    .find({})
    .sort({date: 'desc'})
    .exec()
  promise

newsSchema.statics.get_one_news = (slug) ->
  promise = @
    .find({slug: slug})
    .exec()
  promise

module.exports.News = mongoose.model 'News', newsSchema
