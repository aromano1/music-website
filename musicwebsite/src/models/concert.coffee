countrydata = require('country-data')
moment = require 'moment'
mongoose = require 'mongoose'
sprintf = require 'sprintf'
utils = require '../utils'

Schema = mongoose.Schema
ObjectID = Schema.ObjectID

concertSchema = new Schema
  venue: String
  city: String
  country: String
  date: Date
  date_ends: Date
  slug: String
  urls:
    venue: String
    facebook: String
    photos: String
    songkick: String
    setlist: String
    event: String
  setlist: [
    slug: String
    title: String
    notes: String]
  notes: String
  cancelled:
    type: Boolean
    default: false

# get_properties - translate to context-like object
concertSchema.methods.get_properties = ->
  if @cancelled
    response = null
  else
    date_today = moment({hour: 0})  # Date.now with time = 00:00
    this_date = moment @date
    past_concert = @date < date_today
    time_formatted = this_date.format 'HH:mm'
    if time_formatted == '00:00'
      time_formatted = ''

    if !@date_ends
      date_medium = this_date.format 'DD MMM YYYY'
      date_short = this_date.format 'DD.MM.YY'
    else
      get_date_structure = (date_obj) ->
        date_moment = moment date_obj
        response =
          day: date_moment.format 'DD'
          month_medium: date_moment.format 'MMM'
          month_short: date_moment.format 'MM'
          year_medium: date_moment.format 'YYYY'
          year_short: date_moment.format 'YY'
        response

      date_begins = get_date_structure @date
      date_ends = get_date_structure @date_ends

      if date_begins.year_short != date_ends.year_short  # across a year
        date_short = sprintf.sprintf '%s.%s.%s-%s.%s.%s', date_begins.day, date_begins.month_short, date_begins.year_short, date_ends.day, date_ends.month_short, date_ends.year_short
        date_medium = sprintf.sprintf '%s %s %s-%s %s %s', date_begins.day, date_begins.month_medium, date_begins.year_medium, date_ends.day, date_ends.month_medium, date_ends.year_medium
      else if date_begins.month_short != date_ends.month_short  # across a month
        date_short = sprintf.sprintf '%s.%s-%s.%s.%s', date_begins.day, date_begins.month_short, date_ends.day, date_ends.month_short, date_ends.year_short
        date_medium = sprintf.sprintf '%s %s-%s %s %s', date_begins.day, date_begins.month_medium, date_ends.day, date_ends.month_medium, date_ends.year_medium
      else if date_begins.day != date_ends.day  # more than one day
        date_short = sprintf.sprintf '%s-%s.%s.%s', date_begins.day, date_ends.day, date_ends.month_short, date_ends.year_short
        date_medium = sprintf.sprintf '%s-%s %s %s', date_begins.day, date_ends.day, date_ends.month_medium, date_ends.year_medium
      else  # a one-day event
        date_medium = this_date.format 'DD MMM YYYY'
        date_short = this_date.format 'DD.MM.YY'
        
    setlist_url = null
    setlist_full = null
    
    if past_concert
      setlist_entries = []
      if @setlist_entries
        for setlist_entry in @setlist_entries
          do (setlist_entry) ->
            if setlist_entry.slug
              resp = sprintf.sprintf('<li><a href="/site/lyrics/%s/">%s</a></li>', setlist_entry.slug, setlist_entry.title)
            else
              resp = sprintf.sprintf('<li>%s</li>', setlist_entry.title)
            setlist_entries.push resp
        if setlist_entries.length > 0
          setlist_full = '<ol>' + setlist_entries.join '\n' + '</ol>'
        else
          setlist_full = ''
        
    response =
      venue: @venue
      city: @city
      country: @country
      country_full: countrydata.countries[@country].name
      date_medium: date_medium
      date_short: date_short
      time: time_formatted
      slug: @slug
      urls: utils.dict_without_empty_items @urls
      venue_url: @urls.venue
      event_url: @urls.event
      facebook_url: @urls.facebook
      photos_url: @urls.photos
      songkick_url: @urls.songkick
      setlist_url: @urls.setlist
      setlist: setlist_full
      notes: @notes
      past_concert: past_concert

# get_upcoming_concerts - returns upcoming concert objects
concertSchema.statics.get_upcoming_concerts = ->
  date_today = moment({hour: 0})
  promise = @
    .find({date: {$gte: date_today}, cancelled: false})
    .sort({date: 'asc'})
    .exec()
  promise

# get_past_concerts - returns past concert objects
concertSchema.statics.get_past_concerts = ->
  date_today = moment({hour: 0})
  promise = @
    .find({date: {$lt: date_today}, cancelled: false})
    .sort({date: 'desc'})
    .exec()
  promise
  
# get_one_concert - returns a specific concert object by slug
concertSchema.statics.get_one_concert = (slug) ->
  promise = @
    .findOne({slug: slug, cancelled: false})
    .exec()
  promise

module.exports.Concert = mongoose.model 'Concert', concertSchema
