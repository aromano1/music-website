moment = require 'moment'
mongoose = require 'mongoose'
utils = require '../utils'

Schema = mongoose.Schema
ObjectID = Schema.ObjectID

albumSchema = new Schema
  title: String
  publishing_date: Date
  slug: String
  description: String
  urls:
    bandcamp: String
    soundcloud: String
    noisetrade: String
    jamendo: String
  html_embed: String
  tracks: [
    slug: String
    title: String
    notes: String]
  front_cover:
    small: String
    medium: String
    large: String
  notes: [String]
  type: String

# get_properties - translate to context-like object
albumSchema.methods.get_properties = ->
  response =
    title: @title
    publishing_date: moment(@publishing_date).format 'DD MMMM YYYY'
    slug: @slug
    description: @description
    urls: utils.dict_without_empty_items @urls
    tracks: utils.listify @tracks
    front_cover: utils.dict_without_empty_items @front_cover
    notes: utils.strarray_to_htmltext @notes
    type: @type
    html_embed: @html_embed

  response

albumSchema.statics.get_albums = ->
  promise = @
    .find({})
    .sort({publishing_date: 'desc'})
    .exec()
  promise
  
albumSchema.statics.get_one_album = (slug) ->
  promise = @
    .findOne({slug: slug})
    .exec()
  promise

module.exports.Album = mongoose.model 'Album', albumSchema
