#### Utils
# General purpose util functions
thisutil = require 'util'

# strarray_to_htmltext - converts array of string to a combination of P and BR
module.exports.strarray_to_htmltext = (arr) ->
  content_lines = []
  for content_line in arr
    do (content_line) ->
      if content_line and content_line.length > 0
        content_lines.push('<p>' + content_line + '</p>')
      else
        content_lines.push '<br />'
  content_lines.join '\n'
  
module.exports.dict_without_empty_items = (dic) ->
  response = {}
  if dic
    for own type, val of dic.toObject()
      do (type, val) ->
        if val
          response[type] = val

  if Object.keys(response).length > 0
    response
  else
    null

module.exports.listify = (obj) ->
  if !obj
    []
  else if (thisutil.isArray obj)
    obj
  else
    [obj]
